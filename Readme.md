# gurgulio

## Name

The first project of what can be a series was a sort of Gargoyle. Probably not the original authours intention but that is what it potentially looks like. The worth [gurgulio](https://en.wikipedia.org/wiki/Gargoyle#Etymology) is the latin root for gargoyle and that is what stuck. Yes, it means gargle in latin. 

## Objective

Demonstrate Mix and match as a possiblity for creating new projects. 
Many CAD models exist in file repositories such as [thingyverse](https://alternativeto.net/software/thingiverse/). On these the trick is to know which licence they are released on, give credit where credit is due and either pay or tip the original designers.

There is a lot of help for getting started with CAD software and doing new things but this project is about taking existing things and mashing them toguether. 

## Road map

1. Create a project called Leo. 
2. This project will have detailed instructions and files to be able to reproduce. 
3. Key feature is the need to use different files. Licences should be cc-by-sa. The project will steer clear from non-commercial licences. 
3. The files I am working with are STL files normally so a cloud of dots. For this case I have decided to teach myself blender but I will document how to do one thing, not having to go into other features.
4. Machine/3Dprint the file in different mediums.  
5. repeat with a new project. 

