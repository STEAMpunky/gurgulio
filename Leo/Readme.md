# Steps followed

## Finding files

1. create account in [thingyverse](https://www.thingiverse.com/)
2. create a collection "things to merge" for example
3. add [golem](https://www.thingiverse.com/thing:1269853) that kind of fits requirements. Will need horns removed. Probably scale larger. probably make holes for holding cylindrical objects. 
4. add [helm](https://www.thingiverse.com/thing:25422190) which looks like what is needed. Will need to remove visor and shrink to size.  
6. add [morning Star](https://www.thingiverse.com/thing:2123543) Will need scaling size. 


##virtual assembly

Before we go crazy printing things and seeing how they go toguether it is a good idea to try out the electronic models and check for mesh errors 

1. download collected items, make sure licence is downloaded as well. 
2. Start up FreeCAD
2. Start mode mesh design
3. Meshes>analyze>evaluate and repair mesh
4. Tick bottom option "all above tests toguether" analyze. 
5. Click repair as needed. Close
6. Repeat with other items. Note that if something renders black on FreeCAD then it might be something wrong. For this reason we will ignore the right wing at the moment. 
7. create a new file assembly and import the repaired body and repaired wing
8. use the "fit whole content to screen" button to fit it all.
9. Create a copy of the left wing. open it with the part mode. Turn it into a shape, mirror it. Hide all other compenents and export as stl as a right wing.
10. import this new right wing to the model. 
11. left wing, angle 90 axis, axis (1,0,0), postion (0mm,-2mm,21mm)
12. rightwing, same as above but position x= -3mm

## modifications

after seeing the first part as to how the parts will be assembled we now work on modifing existing parts. We will start with how to add the morning star to the body. 

1. Start with the mesh model of the repaired body. 
2. Add a cylinder mesh meshes>regular solid. radius 0.5mm to start 
3. using the front, lateral and side view position the cylinder as close as possible to one of the arms. in my case this is (x,y,z)=6mm, -33mm, 20 mm
4. using top view adjust angle to 59deg while keeping (0,0,1) in axis. 
4. angles are aligned using a vector and one angle.
5. still on top view adjust (x,y) position to (8mm,-38mm)
6. Changing to back view. Keeping the angle the same adjust x axis to that the cylinder fits in the hand. axis(-0.29,0,0.96)
7. Change to right view. adjust angle and postion. 
8. repeat 3 previous steps until happy with position.
9. alternatively. select cylinder and go to edit> Placement... 
10. the task menu you can choose euler angles. 3 different angles. 
11. my result: (x,y,z) = 9.5,-37,20.5;  euler= 69deg, 11deg, 0deg

BROKE

At this point I discovered that even though my part was repaired I could not do a bolean operation. This could be an issue of my instalation. 

I will try openscad directly. 
 











2. start up blender
3. click on the screen to remove presenation, use right mouse click and press "delete" in keyboard. window pops up, delete it. 
3. file > import > stl file and look for one of the items.



