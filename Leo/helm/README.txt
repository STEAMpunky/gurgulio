                   .:                     :,                                          
,:::::::: ::`      :::                   :::                                          
,:::::::: ::`      :::                   :::                                          
.,,:::,,, ::`.:,   ... .. .:,     .:. ..`... ..`   ..   .:,    .. ::  .::,     .:,`   
   ,::    :::::::  ::, :::::::  `:::::::.,:: :::  ::: .::::::  ::::: ::::::  .::::::  
   ,::    :::::::: ::, :::::::: ::::::::.,:: :::  ::: :::,:::, ::::: ::::::, :::::::: 
   ,::    :::  ::: ::, :::  :::`::.  :::.,::  ::,`::`:::   ::: :::  `::,`   :::   ::: 
   ,::    ::.  ::: ::, ::`  :::.::    ::.,::  :::::: ::::::::: ::`   :::::: ::::::::: 
   ,::    ::.  ::: ::, ::`  :::.::    ::.,::  .::::: ::::::::: ::`    ::::::::::::::: 
   ,::    ::.  ::: ::, ::`  ::: ::: `:::.,::   ::::  :::`  ,,, ::`  .::  :::.::.  ,,, 
   ,::    ::.  ::: ::, ::`  ::: ::::::::.,::   ::::   :::::::` ::`   ::::::: :::::::. 
   ,::    ::.  ::: ::, ::`  :::  :::::::`,::    ::.    :::::`  ::`   ::::::   :::::.  
                                ::,  ,::                               ``             
                                ::::::::                                              
                                 ::::::                                               
                                  `,,`


https://www.thingiverse.com/thing:2542219
Skyrim Iron Helm by GliArtigianiDiAvalon is licensed under the Creative Commons - Attribution license.
http://creativecommons.org/licenses/by/3.0/

# Summary

This is a fan-made model of the famous iron helmet from the Bethesda video game: TES V: skyrim. We shaped the various details of the helm following the ones on the textures and on the normal maps in the game. 

Please feel free to give us a tip in our public profile here on thingiverse, we would be grateful to you.

The helmet is shaped for a relatively large head, we invite you to follow this table to re-scale it according to your needs. (NOTE: These values ​​are only indicative, we invite you to take the measurements on yourself and scale it as needed).
94% = Small
97% = Medium
100% = Wide
103% = Extra Large

Have a good printing time!

Gli Artigiani di Avalon

# Print Settings

Printer Brand: Prusa
Printer: Prusa Clone
Rafts: No
Supports: Yes
Resolution: 250
Infill: 10%

# Post-Printing

## HOW TO COLOR:

Just give a tick layer of Black plastic primer, then a Spray Chrome layer.
Leave it Dry.
Next day give a layer of dark metal spray. Once dryed, you can scratch the dark colour revealing the chrome from the previous layer.
If you wish, you can put some rust in little spot around the rivets or the eyes.

For the horns, use at least 3 different shades of sand.

We suggest you to darken areas were 2 or more part are close together, in order to recreate a sort of Ambient Occlusion (use dry brush technique or aribrush).

Final: Use a Clear Glossy transparent acrylic spray all over the helmet, leaving the horns matte.